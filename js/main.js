'use strict'

/**
 * Теорія:
 * Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
 * Механізм прототипного наслідування створює у об'єкта (класа) нащадка посилання на свої публічні методи,
 * таким чином, вони стають доступні нащадкам без копіювання.
 *
 * Для чого потрібно викликати super() у конструкторі класу-нащадка?
 * Для виклику конструктора батьківського класу.
 *
 * Задача:
 * Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
 * Створіть гетери та сеттери для цих властивостей.
 * Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
 * Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
 * Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
 * */

class Employee {

    constructor(name, age, salary) {
        this.name = name,
        this.age = age,
        this.salary = salary;
    }

    getName() { return this.name; }
    getAge() { return this.age; }
    getSal() { return this.salary; }

    setName(value) { this.name = value; }
    setAge(value) { this.age = value; }
    setSal(value) { this.salary = value; }

}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary),
        this.languages = lang;
    }

    getSal() { return this.salary * 3 ; }
}


const igor = new Programmer('Igor', 18, 2000, ['eng', 'ua', 'ru'])
const alex = new Programmer('Alex', 16, 2000, ['eng', 'ua', 'ru'])
const artem = new Programmer('Artem', 7, 2000, ['eng', 'ua', 'ru'])
const uasia = new Employee('Uasia', 7, 1000)

console.log(igor, alex, artem, uasia);
console.log(igor.getSal());
console.log(uasia.getSal());


